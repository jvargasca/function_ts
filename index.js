"use strict";
exports.__esModule = true;
var funciones_1 = require("./funciones");
var funciones_2 = require("./funciones");
var funciones_3 = require("./funciones");
var funtions_1 = require("./funtions");
var funtions_2 = require("./funtions");
var funtions_3 = require("./funtions");
var funtions_4 = require("./funtions");
var fun_1 = require("./fun");
var fun_2 = require("./fun");
var fun_3 = require("./fun");
/*jose*/
console.log("\n");
console.log("josé");
console.log("suma:", (0, fun_1.sumas)(23, 42));
console.log("resta:", (0, fun_2.restar)(100, 50));
console.log("multiplicacion:", (0, fun_3.mul)(10, 10, 1));
/*monika*/
console.log("\n");
console.log("monica");
console.log("division:", (0, funtions_4.divd)(45, 89));
console.log("resta:", (0, funtions_3.restas)(23, 57));
console.log("resta:", (0, funtions_2.resta)(76, 34));
console.log("suma:", (0, funtions_1.suma)(54, 65));
/*cesar*/
console.log("\n");
console.log("cesar");
console.log("suma:", (0, funciones_1.sum)(4, -4));
console.log("resta:", (0, funciones_2.rest)(50, 100));
console.log("string:", (0, funciones_3.str)("hey", "you"));

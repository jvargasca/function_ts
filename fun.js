"use strict";
exports.__esModule = true;
exports.mul = exports.restar = exports.sumas = void 0;
function sumas(valor1, valor2) {
    return valor1 + valor2;
}
exports.sumas = sumas;
function restar(valor1, valor2) {
    return valor1 - valor2;
}
exports.restar = restar;
function mul(valor1, valor2, valor3) {
    return valor1 * valor2 * valor3;
}
exports.mul = mul;

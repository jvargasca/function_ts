"use strict";
exports.__esModule = true;
exports.divd = exports.restas = exports.resta = exports.suma = void 0;
function suma(valor1, valor2) {
    return valor1 + valor2;
}
exports.suma = suma;
function resta(valor1, valor2) {
    return valor1 - valor2;
}
exports.resta = resta;
function restas(valor1, valor2) {
    return valor1 - valor2;
}
exports.restas = restas;
function divd(valor1, valor2) {
    return valor1 / valor2;
}
exports.divd = divd;
